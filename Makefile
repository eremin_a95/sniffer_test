BIN = sniffer-test
LIB = -lpcap -lyaml
OPT = -O3

all: $(BIN)

parse.c:
	gcc $@ $(OPT)

sniffer-test: parse.c
	gcc main.c $^ $(OPT) $(LIB) -o $@

clean:
	rm -f $(BIN)
