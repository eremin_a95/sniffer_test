#include <stdio.h>
#include <pcap.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/udp.h>

#include "parse.h"

void main_loop(pcap_t *handle) {
	struct pcap_pkthdr header;
	const u_char *packet;

	struct ethhdr *eth;
	struct iphdr *ip;
	struct ipv6hdr *ip6;
	void *nexthdr;
	struct tcphdr *tcp;
	struct udphdr *udp;
	char src[INET6_ADDRSTRLEN], dst[INET6_ADDRSTRLEN];
	int proto;
	uint16_t src_port, dst_port;
	char proto_str[8];
	float size;

	while(1){
		packet = pcap_next(handle, &header);
		size = (float)header.len / 1024; 
		eth = (struct ethhdr *)packet;
		switch (ntohs(eth->h_proto)){
		case ETH_P_IP:
			ip = (struct iphdr *)(packet + sizeof(struct ethhdr));
			inet_ntop(AF_INET, &ip->saddr, src, INET_ADDRSTRLEN);
			inet_ntop(AF_INET, &ip->daddr, dst, INET_ADDRSTRLEN);
			proto = ip->protocol;
			nexthdr = (void *)(ip + 1);
			break;
		case ETH_P_IPV6:
			ip6 = (struct ipv6hdr *)(packet + sizeof(struct ethhdr));
			inet_ntop(AF_INET6, &ip6->saddr, src, INET6_ADDRSTRLEN);
			inet_ntop(AF_INET6, &ip6->daddr, dst, INET6_ADDRSTRLEN);
			proto = ip6->nexthdr;
			nexthdr = (void *)(ip6 + 1);
			break;
		default:
			continue;
		}
		switch (proto){
		case IPPROTO_TCP:
			strcpy(proto_str, "TCP");
			tcp = (struct tcphdr *)nexthdr;
			src_port = ntohs(tcp->source);
			dst_port = ntohs(tcp->dest);
			break;
		case IPPROTO_UDP:
			strcpy(proto_str, "UDP");
			udp = (struct udphdr *)nexthdr;
			src_port = ntohs(udp->source);
			dst_port = ntohs(udp->dest);
			break;
		default:
			printf("%s -> %s (proto %d), %0.2f KiB\n",
				src, dst, proto, size);
			continue;
		}
		printf("%s:%d -> %s:%d (proto %d-%s), %0.2f KiB\n",
			src, src_port, dst, dst_port, proto, proto_str, size
		);
	}
}

void usage() {
	puts("Usage:");
	puts("  insis-test -i <ifname>");
	puts("             [-saddr <addr1[,addr2...]>");
	puts("              -dport <port1[,port2...]>");
	puts("              -proto <proto1[,proto2...]>]");
	puts("where:");
	puts("  <ifname> - set name of interface to sniff");
	puts("  <addr1[,addr2...]> - filter by source address");
	puts("    You can use both IPv4 and IPv6 addresses");
	puts("  <port1[,port2...]> - filter by destination port");
	puts("  <proto1[,proto2...]> - filter by protocol number");
	puts("    You can use protocol numbers and/or keywords:");
	puts("      ip, ip6, icmp, udp, tcp");
	puts("");
	puts("Or use:");
	puts("  insis-test -c <filename>");
	puts("where");
	puts("    <filename> - parse parameters from YAML file.");
	puts("Format of YAML config (args is the same as above):");
	puts("  ifname: <ifname>");
	puts("  saddr: <saddr1[,saddr2...]>");
	puts("  dport: <port1[,port2...]>");
	puts("  proto: <proto1[,proto2...]>");
}

int main(int argc, char **argv) {
	int ret;
	pcap_t *handle;
	char errbuf[PCAP_ERRBUF_SIZE];
	char filter_exp[1024], ifname[64];
	struct bpf_program fp;

	if ((argc % 2 == 0) || (argc < 3)){
		usage();
		return 0;
	}

	if (ret = parse_args(argc, argv, filter_exp, ifname)){
		fprintf(stderr, "Error occures while parsing arguments, error code %d\n", ret);
		usage();
		return ret;
	}
	//printf("Sniffing on interface %s with filter %s\n", ifname, filter_exp);

	handle = pcap_open_live(ifname, BUFSIZ, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "Can't open device %s: %s\n", argv[2], errbuf);
		return -1000;
	}

	if (pcap_compile(handle, &fp, filter_exp, 0, PCAP_NETMASK_UNKNOWN) == -1) {
		fprintf(stderr, "Can't compile filter %s: %s\n", filter_exp, pcap_geterr(handle));
		return -1001;
	}
	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Can't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
		return -1002;
	}
	main_loop(handle);
}
