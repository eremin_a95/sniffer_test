# sniffer-test

## Install

To install application clone this repo, move to it's directory and do `make` command.

You need gcc compiler, libyaml and libpcap libraries to compile binary file successfully.

Install example for `apt-get` Linux distro:

	sudo apt-get install -y gcc libyaml-dev libpcap-dev
    git clone https://gitlab.com/eremin_a95/sniffer_test
    cd sniffer-test
    make
    ./sniffer-test

## Usage

	sniffer-test -i <ifname> [-saddr <addr1[,addr2...]> -dport <port1[,port2...]> -proto <proto1[,proto2...]>

where:

`<ifname>` - set name of interface to sniff
   
`<addr1[,addr2...]>` - filter by source address, you can use both IPv4 and IPv6 addresses
    
`<port1[,port2...]>` - filter by destination port
  
`<proto1[,proto2...]>` - filter by protocol number, you can use protocol numbers and/or keywords: `ip`, `ip6`, `icmp`, `udp`, `tcp`

Or use:

	sniffer-test -c <filename>

where

`<filename>` - parse parameters from YAML file

Format of YAML config (args is the same as above):

	ifname: <ifname>
	saddr: <saddr1[,saddr2...]>
    dport: <port1[,port2...]>
	proto: <proto1[,proto2...]>
    
## Examples

Interface name in examples is "eth0"
   
### From IPs 192.168.0.100 and 192.168.0.200 to destination ports 80 and 8080

    sniffer-test -i eth0 -saddr 192.168.0.100,192.168.0.200 -dport 80,8080
    
### All UDP to port 53

    sniffer-test -i eth0 -dport 53 -proto udp
    
### All IPv6

    sniffer-test -i eth0 -proto ip6