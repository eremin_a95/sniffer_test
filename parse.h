#include <stdio.h>
#include <string.h>

int parse_filter_pair(char *arg1, char *arg2, char *filter_exp);

int parse_args(int argc, char **argv, char *filter_exp, char *ifname);

int parse_args_config(const char *filename, char *filter_exp, char *ifname);
