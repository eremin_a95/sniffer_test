#include <yaml.h>

#include "parse.h"

int parse_args_config(const char *filename, char *filter_exp, char *ifname) {
	int ret = 0;
	int state = 0;
	yaml_parser_t parser;
	yaml_token_t token;
	char key[128];
	FILE *file = fopen(filename, "rt");
	if (file == NULL) {
		fprintf(stderr, "Can't open file %s!\n", filename);
		return -2000;
	}
	if (!yaml_parser_initialize(&parser)) {
		fprintf(stderr, "Can't initialize YAML parser!\n");
		ret = -2001;
		goto close;
	}
	yaml_parser_set_input_file(&parser, file);

	do {
		char *data;
		yaml_parser_scan(&parser, &token);
		switch(token.type){
		case YAML_KEY_TOKEN:	state = 0; break;
		case YAML_VALUE_TOKEN:	state = 1; break;
		case YAML_SCALAR_TOKEN:
			data = token.data.scalar.value;
			if (state == 0) {
				strcpy(key, data);
			} else {
				if (!strcmp(key, "ifname"))
					strcpy(ifname, data);
				else {
					parse_filter_pair(key, data, filter_exp);
					strcat(filter_exp, " and ");
				}
			}
		default: break;
		}
	} while(token.type != YAML_STREAM_END_TOKEN);
	yaml_token_delete(&token);
	filter_exp[strlen(filter_exp) - 5] = '\0'; //Cut last " and "

	yaml_parser_delete(&parser);
close:
	fclose(file);
	return ret;
}

int parse_filter_pair(char *arg1, char *arg2, char *filter_exp) {
	char *str, *filter_key;
	if (!strcmp(arg1, "proto")){
		strcat(filter_exp, "(");
		str = strtok(arg2, ",");
		do {
			if (!strcmp(str, "tcp")
				|| (!strcmp(str, "ip6"))
				|| (!strcmp(str, "ip"))
				|| (!strcmp(str, "udp"))
				|| (!strcmp(str, "icmp"))){
					strcat(filter_exp, str);
			} else {
				strcat(filter_exp, "(ip proto ");
				strcat(filter_exp, str);
				strcat(filter_exp, " or ip6 proto ");
				strcat(filter_exp, str);
				strcat(filter_exp, ")");
			}
			strcat(filter_exp, " or ");
		} while ((str = strtok(NULL, ",")) != NULL);
		filter_exp[strlen(filter_exp) - 3] = '\0'; //Cut last " or "
		strcat(filter_exp, ")");
		return 0;
	}


	if (!strcmp(arg1, "saddr"))
		filter_key = "src ";
	else if (!strcmp(arg1, "dport"))
		filter_key = "dst port ";
	else
		return 1;

	strcat(filter_exp, "(");
	strcat(filter_exp, filter_key);
	str = strtok(arg2, ",");
	strcat(filter_exp, str);
	while ((str = strtok(NULL, ",")) != NULL) {
		strcat(filter_exp, " or ");
		strcat(filter_exp, str);
	}
	strcat(filter_exp, ")");
	return 0;
}

int parse_args(int argc, char **argv, char *filter_exp, char *ifname) {
	int i;
	filter_exp[0] = '\0';
	for (i = 1; i + 1 < argc; i+=2) {
		if (!strcmp(argv[i], "-c")){
			return parse_args_config(argv[i+1], filter_exp, ifname);
		}

		if (!strcmp(argv[i], "-i")){
			strcpy(ifname, argv[i+1]);
			continue;
		}

		if (strlen(filter_exp) > 0)
			strcat(filter_exp, " and ");

		if (parse_filter_pair(argv[i]+1, argv[i+1], filter_exp)) {
			fprintf(stderr, "Unknown arg key: %s\n", argv[i]);
			filter_exp[strlen(filter_exp) - 5] = '\0'; //Cut last " and "
		}
	}
	return 0;
}
